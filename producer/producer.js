console.log("producer..");

import Kafka from "node-rdkafka";
import eventType from "../eventType.js";

const stream = Kafka.Producer.createWriteStream(
  {
    "metadata.broker.list": "localhost:9092",
  },
  {},
  {
    topic: "test",
  }
);

function getRandomAnimal(){
    const catagories=['CAT','DOG'];
    return catagories[Math.floor(Math.random()*catagories.length)]
}

function getRandomNoise(animal){
    if(animal==='CAT'){
        const noises=['purr','meow'];
        return noises[Math.floor(Math.random()*noises.length)];
    } 
    else if(animal==='DOG'){
        const noises=['bark','woo'];
        return noises[Math.floor(Math.random()*noises.length)];
    }
}

function queueMessage() {
    const category=getRandomAnimal();
    const noise =getRandomNoise(category)
  const event = { category, noise };
  const result = stream.write(eventType.toBuffer(event));
  if (result) {
    console.log("message wrote successfully");
  } else {
    console.log("something went wrong");
  }
}
setInterval(() => {
  queueMessage();
}, 3000);
